public with sharing class GroupFeedController{
    
    private static String communityId = '0DBj0000000Kzae';
    
    public String groupId { get; set; }

    public String groupName {get {
        return ConnectApi.ChatterGroups.getGroup(communityId, ConnectApi.ChatterFeeds.getComment(communityId, System.currentPageReference().getParameters().get('cid')).Parent.Id).Name;
        } set;}

    public List<ConnectApi.ChatterGroup> getChatterGroups() {
        return ConnectApi.ChatterGroups.getGroups(communityId).groups;
    }
    
    public ConnectApi.ChatterGroupDetail getGroupDetail() {
        String groupId = System.currentPageReference().getParameters().get('gid');
        return  ConnectApi.ChatterGroups.getGroup(communityId, groupId);
    }

    public List<ConnectApi.FeedItem> getFeedItems() {
        String groupId = System.currentPageReference().getParameters().get('gid');
        try {
            if (String.isEmpty(groupId)) { return null; }
            List<ConnectApi.FeedItem> feedItems = new List<ConnectApi.FeedItem>();
            List<ConnectApi.FeedElement> feedElements = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(communityId, ConnectApi.FeedType.Record, groupId).elements;
            for(ConnectApi.FeedElement fed : feedElements) {
                ConnectApi.FeedItem fet = (ConnectApi.FeedItem)fed;
                if(fet.Type == ConnectApi.FeedItemType.TextPost) feedItems.add(fet);
            }
            return feedItems;
        } catch(Exception e) {ApexPages.addMessages(e); return null; }
    }

    public ConnectApi.FeedItem getFeedItemDetail() {
        String feedItemId = System.currentPageReference().getParameters().get('fid');
        return  (ConnectApi.FeedItem)ConnectApi.ChatterFeeds.getFeedElement(communityId, feedItemId);
    }

    public List<ConnectApi.Comment> getComments() {
        String feedItemId = System.currentPageReference().getParameters().get('fid');
        try {
            if (String.isEmpty(feedItemId)) { return null; }
            return ConnectApi.ChatterFeeds.getCommentsForFeedElement(communityId, feedItemId).items;
        } catch(Exception e) {ApexPages.addMessages(e); return null; }
    }

    public ConnectApi.Comment getCommentDetail() {
        String commentId = System.currentPageReference().getParameters().get('cid');
        return ConnectApi.ChatterFeeds.getComment(communityId, commentId);
    }
    
}