global class SEORewriterClass implements Site.UrlRewriter {
	String GROUP_PAGE = '/group/';
	String GROUP_VISUALFORCE_PAGE ='/GroupDetailPage?gid=';
	String ITEM_PAGE = '/feeditem/';
	String ITEM_VISUALFORCE_PAGE ='/FeedItemDetailPage?fid=';
	String COMMENT_PAGE = '/comment/';
	String COMMENT_VISUALFORCE_PAGE ='/CommentDetailPage?cid=';
	String SITE_PAGE = '/sitemap';
	String SITE_VISUALFORCE_PAGE ='/SEOSiteMap';
	String ROBOTS_TXT = '/robots.txt';
	String ROBOTS_VISUALFORCE_PAGE ='/SEORobotsTxt';
	global PageReference mapRequestUrl(PageReference myFriendlyUrl) {
	    String url = myFriendlyUrl.getUrl();
	    System.debug(url);
	    /*if(url.contains(GROUP_PAGE) && url.contains(ITEM_PAGE)) {
	       List<String> tokens = url.split('/');
	       String id = tokens.get(tokens.size()-1);
	       FeedItem fi = [SELECT Id FROM FeedItem WHERE Id =:id LIMIT 1];
	       return new PageReference(GROUP_VISUALFORCE_PAGE+ '0D5j000000CGlEkCAL');
	   	}*/
	   	if(url.startsWith(GROUP_PAGE) && !url.contains(ITEM_PAGE) && !url.contains(COMMENT_PAGE)) {
	       String name = url.substring(GROUP_PAGE.length(),url.length());
	       CollaborationGroup cg = [SELECT Id FROM CollaborationGroup WHERE Name =:name LIMIT 1];
	       return new PageReference(GROUP_VISUALFORCE_PAGE+ cg.id);
	    }
	    if(url.startsWith(GROUP_PAGE) && url.contains(ITEM_PAGE) && !url.contains(COMMENT_PAGE)) {
	       List<String> tokens = url.split('/');
	       String id = FeedItem.sObjectType.getDescribe().getKeyPrefix()+tokens.get(tokens.size()-1);
	       FeedItem fi = [SELECT Id FROM FeedItem WHERE Id =:id LIMIT 1];
	       return new PageReference(ITEM_VISUALFORCE_PAGE+ fi.id);
	    }
	    if(url.startsWith(GROUP_PAGE) && url.contains(ITEM_PAGE) && url.contains(COMMENT_PAGE)) {
	       List<String> tokens = url.split('/');
	       String id = FeedComment.sObjectType.getDescribe().getKeyPrefix() +tokens.get(tokens.size()-1);
	       FeedComment fc = [SELECT Id FROM FeedComment WHERE Id =:id LIMIT 1];
	       return new PageReference(COMMENT_VISUALFORCE_PAGE+ fc.id);
	    }
	    if(url.contains(SITE_PAGE)) return new PageReference(SITE_VISUALFORCE_PAGE);
	    if(url.contains(ROBOTS_TXT)) return new PageReference(ROBOTS_VISUALFORCE_PAGE);
	    return null;
	}



  // The second global method for mapping internal Ids to URLs
  global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls) {
    return mySalesforceUrls;
  }
}